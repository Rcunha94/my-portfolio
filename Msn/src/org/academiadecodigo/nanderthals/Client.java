package org.academiadecodigo.nanderthals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        String hostName = "localhost";
        int serverPort = 8080;

        try {
            Socket clientSocket = new Socket(hostName, serverPort);
            System.out.println("Connected to server: " + clientSocket);

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            Scanner scanner = new Scanner(System.in);

            // Start a separate thread to listen for server messages
            Thread serverListener = new Thread(() -> {
                try {
                    String serverMessage;
                    while ((serverMessage = in.readLine()) != null) {
                        System.out.println("Message from server: " + serverMessage);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            serverListener.start();

            // Read user input and send messages to the server
            String userInput;

            while (!(userInput = scanner.nextLine()).equalsIgnoreCase("/quit")) {
                out.println(userInput);
            }
            out.println("Client has logged out");

            // Disconnect from the server
            clientSocket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


