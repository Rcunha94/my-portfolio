package org.academiadecodigo.nanderthals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static final int PORT = 8080;
    private static List<ServerWorker> clients = new ArrayList<>();
    private static ExecutorService fixedPool = Executors.newFixedThreadPool(4);

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            System.out.println("Server started on port " + PORT);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("New client connected: " + clientSocket.getInetAddress().getHostAddress());

                ServerWorker worker = new ServerWorker(clientSocket, clients);
                clients.add(worker);
                fixedPool.execute(worker);

                String inputLine = null;
                if ((inputLine = worker.readMessage()) != null) {
                    broadcastMessage(inputLine);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void broadcastMessage(String message) {
        for (ServerWorker worker : clients) {
            worker.sendMessage(message);


        }
    }
}