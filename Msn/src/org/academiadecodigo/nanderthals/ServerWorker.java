package org.academiadecodigo.nanderthals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

public class ServerWorker implements Runnable {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private List<ServerWorker> clients;
    private String username;
    public ServerWorker(Socket clientSocket, List<ServerWorker> clients) {
        this.clientSocket = clientSocket;
        this.clients = clients;
        this.username = username;
        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void run() {
        try {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println("Received message from client: " + inputLine);

                if (inputLine.startsWith("/")) {
                    // Handle command
                    String command = inputLine.substring(1);
                    handleCommand(command);
                } else if(inputLine.startsWith("/list")) {
                   // o que posso aqui meter ????
                } else {
                    // Broadcast message to all connected clients
                    //  broadcastMessage(inputLine);
                }
            }

            // Client disconnected
            System.out.println("Client disconnected: " + clientSocket.getInetAddress().getHostAddress());
            clients.remove(this);
            clientSocket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleCommandList(int port) {//tentativa de aparecer o porto do pessoal
    out.print(port);

    }

    private void handleCommand(String command) {
        if (command.equalsIgnoreCase("quit")) {
            String clientName = clientSocket.getInetAddress().getHostAddress();
            System.out.println("Client has logged out: " + clientName);
            whisper(clientName + " has logged out");
            return;
        } else if (command.equalsIgnoreCase("list")) {
            String usernames = getClientUsernames();
            whisper("Connected usernames: " + usernames);
            return;

        }
    }

    private void whisper(String message) {
        sendMessage("[" + clientSocket.getInetAddress().getHostAddress() + "]: " + message);
    }

    public String readMessage () {
            try {
                return in.readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    private String getClientUsernames() {
        StringBuilder usernames = new StringBuilder();
        for (ServerWorker worker : clients) {
            String username = worker.getUsername();
            if (username != null) {
                usernames.append(username).append(", ");
            }
        }
        if (usernames.length() > 0) {
            usernames.delete(usernames.length() - 2, usernames.length());
        }
        return usernames.toString();
    }

    private String getUsername() {
        // Implemente a lógica para obter o username do cliente associado a este ServerWorker
        // Pode ser através de um atributo adicionado à classe ou obtendo-o de outra fonte
        return username;
    }
        void sendMessage (String message){
            out.println(message);
        }
    }









